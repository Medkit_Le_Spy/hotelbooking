
import Model.Booking;
import java.io.IOException;
import java.time.LocalDate;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Naifira
 */
public class BookingViewController {
    private Booking booking;
    @FXML
    private AnchorPane content;

    @FXML
    private Button confirmBookingButton;
    
    @FXML
    private DatePicker checkInDate;
    @FXML
    private DatePicker checkOutDate;
    @FXML
    private Label priceLabel;
    
    
    public void initData(int roomID){
        booking = new Booking();
        booking.setRoomID(roomID);
        checkInDate.setValue(LocalDate.now());
        
        
    }
    
    @FXML
    private void confirmBookingButtonAction() throws IOException{
        content.getScene().getWindow().hide();
        this.showBookingConfirmationView();
    }
    
    private void showBookingConfirmationView() throws IOException{
        FXMLLoader loader = new FXMLLoader(
            getClass().getResource(
                        "BookingConfirmationView.fxml"
                       )
        );
        Stage stage = new Stage();
        stage.setScene(
            new Scene(
                (Pane) loader.load()
            )
        );
        
        stage.setTitle("Confirmation");
        BookingConfirmationViewController controller = 
                loader.<BookingConfirmationViewController>getController();
        
        controller.initData(checkInDate.getValue().toString());
       
        stage.show();
    }
    
}
