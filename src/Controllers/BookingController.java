package Controllers;

import Model.Booking;
import Model.Service;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Controls all booking operations
 * @author Michail Krugliakov 100136484
 */
public class BookingController extends Controller{
    
    public static final String BOOKING_FIELDS = " BOOKING (userID, roomID, "
                                              + "checkIn, checkOut, service,"
                                              + "discountCode, noOfGuest) ";
    public BookingController(){}
    
    /**
     * Returns the booking information
     * @param bookingID - id of booking
     * @return - booking
     * @throws java.sql.SQLException
     */
    public Booking getBooking(int bookingID) throws SQLException{
        Booking booking = null;
        String statement="SELECT * FROM BOOKING where bookingID="+bookingID+";";
        ResultSet rs = this.executeSQLResultSetStatement(statement);
        
        if(rs.next()){
            int id = rs.getInt("bookingID");
            int userID = rs.getInt("userID");
            int roomID = rs.getInt("roomID");
            Date checkIn = rs.getDate("checkIn");
            Date checkOut = rs.getDate("checkOut");
            //Service service = rs.getString("service");
            String dsCode = rs.getString("discountCode");
            int noOfGuest = rs.getInt("noOfGuest");
            booking = new Booking(id,userID,roomID,checkIn, checkOut,null,
                                  dsCode,noOfGuest);
        }
        stmt.close();
        c.close();
        return booking;
    }
    
    /**
     * Adds a booking to the database
     * @param userID
     * @param roomID
     * @param checkIn
     * @param checkOut
     * @param service
     * @param disCode
     * @param noOfGuest
     */
    public void addBooking(int userID, int roomID, Date checkIn, Date checkOut,
                           Service service, String disCode, int noOfGuest){
    
        String values = 
                "("+userID+","
                   +roomID+","
                   +checkIn+","
                   +checkOut+","
               +"'"+service+"'"+","
               +"'"+disCode+"'"+","
                   +noOfGuest+");";
        
        String statement = "INSERT INTO" + BOOKING_FIELDS +
                           "VALUES"     + values; 
        
        this.executeSQLQueryStatement(statement);
        
        System.out.println("Added booking: " + values);
    }
    
    /**
     * Update booking information WIP !!!!!!!!!!!!!!!!!
     */
    public void updateBooking(){

        System.out.println("Updated booking with id = ");
    }
    
    /**
     * Removes booking from database
     * @param bookingID - id to find booking
     */
    public void removeBooking(int bookingID){
        String statement = "DELETE from BOOKING where bookingID="+bookingID+";";
        
        this.executeSQLQueryStatement(statement);
        
        System.out.println("Removed booking with id = "+bookingID);
    }
    /**
     * List all the dates on which a room is booked, if any
     * @param roomID - id of room in bookings
     * @return - list of days when room is occupied
     * @throws SQLException 
     */
    public List<Date> listRoomOccupiedDates(int roomID) throws SQLException{
        List<Date> occDates = new ArrayList<>();
        String statement = "SELECT checkIn, checkOut FROM BOOKING where "
                            + "roomID="+roomID+";";
        ResultSet rs = this.executeSQLResultSetStatement(statement);
        
        while(rs.next()){
            Date st = rs.getDate("checkIn");
            Date en = rs.getDate("checkOut");
            occDates.addAll(BookingController.listDaysBetween(st, en));
        }
        
        stmt.close();
        c.close();
        return occDates;
    }
    
    public double totalPrice(Date in, Date out, int roomID) throws SQLException{
        double price = 0;
        String statement = "SELECT roomPrice FROM ROOM where roomID="+roomID+";";
        ResultSet rs = this.executeSQLResultSetStatement(statement);
        
        if(rs.next()){
            price = 
                rs.getInt("roomPrice") * 
                BookingController.listDaysBetween(in, out).size();
        }
        
        return price;
    }
    
    /**
     * Lists all days between st and en inclusive
     * @param st - start date
     * @param en - end date
     * @return - list all days between the two
     */
    private static List<Date> listDaysBetween(Date st, Date en){
        List<Date> totalDates = new ArrayList<>();
        while(!st.after(en)){
            totalDates.add(st);
            LocalDateTime.from(st.toInstant()).plusDays(1);
        }
        return totalDates;
    }
}
