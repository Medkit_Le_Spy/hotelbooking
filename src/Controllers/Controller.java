package Controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Master controller class that implements all the basic functions and fields
 * needed by any controller.
 * @author Michail Krugliakov 100136484
 */
public abstract class Controller {
    //the default path to the database of hotel booking
    protected static String PATH_TO_DATABASE = "./src/Database/HotelBooking.db";
    protected Connection c = null;
    
    protected Statement stmt = null;
    
    /**
     * Executes any valid sqlite statement for the hotel's database
     * @param statement - valid sqlite statement to be executed
     */
    protected void executeSQLQueryStatement(String statement){
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:"+PATH_TO_DATABASE);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            String sql = statement;
     
            stmt.executeUpdate(sql);

            stmt.close();
            c.commit();
            c.close();
      } catch ( ClassNotFoundException | SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
      }  
    }
    
    /**
     * Returns usable row data. stmt and c need to be closed manually after
     * execution
     * @param statement - sql statement to be executed
     * @return - usable result set data
     */
    protected ResultSet executeSQLResultSetStatement(String statement){
        ResultSet rs= null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:"+PATH_TO_DATABASE);
            c.setAutoCommit(false);

            stmt = c.createStatement();
            rs = stmt.executeQuery( statement );
            
        }catch ( ClassNotFoundException | SQLException e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        return rs;
    }
}
