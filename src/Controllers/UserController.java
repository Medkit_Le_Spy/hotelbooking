package Controllers;

import Model.User;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Interacts with the database to add, edit and delete users, along with
 * extra functionality like finding a specific user.
 * @author Michail Krugliakov 100136484
 */
public class UserController extends Controller{
    //minimum number of digits in userID
    private final static int DIGITS_IN_USERID = 8;
    private final static String USER_FIELDS = " USER (userID, fName, sName, "
                                              + "email, cardID, userTypeID) ";
    
    //list of all userIDs in the database
    private final List<Integer> userIDList;
    
    public UserController() throws SQLException{
        userIDList = this.listAllUserID();
    }
    
    /**
     * Returns the user information
     * @param userID - id of user to be returned
     * @return - user
     * @throws java.sql.SQLException
     */
    public User getUser(int userID) throws SQLException{
        User us = null;
        String statement = "SELECT * FROM USER where userID="+userID+";";
        ResultSet rs = this.executeSQLResultSetStatement(statement);
        
        if(rs.next()){
            int id = rs.getInt("userID");
            String fName = rs.getString("fName");
            String lName = rs.getString("sName");
            String email = rs.getString("email");
            int cardID  = rs.getInt("cardID");
            int userTypeID = rs.getInt("userTypeID");
            us = new User(id, fName, lName, email, cardID, userTypeID);
        }
        stmt.close();
        c.close();
        return us;
    }
    
    /**
     * Adds a user to the database
     * @param fName - user's first name
     * @param lName - last name
     * @param email - email address to send receipts to
     * @param userTypeID - the type of user(eg 4 is guest, 3 is customer)
     */
    public void addUser(String fName, String lName, String email, 
                        int userTypeID){
        //generate new id to identify new user
        int userID = this.generateUserID();
        String values = 
                " ("+userID+","
                +"'"+fName+"'"+","
                +"'"+lName+"'"+","
                +"'"+email+"'"+","
                +0+","
                +userTypeID+");";
        
        String statement = "INSERT INTO" + USER_FIELDS +
                           "VALUES"     + values; 
        
        this.executeSQLQueryStatement(statement);
        
        System.out.println("Added user: " + values);
    }
    
    /**
     * Update user profile 
     * @param userID - id of user to be updated
     * @param fName - first name
     * @param lName - last name
     * @param email - email
     * @param cardID - cardID (if any)
     * @param userTypeID - type of user
     */
    public void updateUser(int userID,String fName, String lName, String email, 
                           int cardID, int userTypeID){
        String statement =  "UPDATE USER set"    +
                                  " fName = '"   +fName     +"',"+
                                  " sName = '"   +lName     +"',"+
                                  " email = '"   +email     +"',"+
                                 " cardID = "    +cardID    +"," +
                             " userTypeID = "    +userTypeID+
                            " where userID = "   +userID    +";";
        
        this.executeSQLQueryStatement(statement);
        System.out.println("Updated user with id = "+userID);
    }
    
    /**
     * Removes user from database
     * @param userID - id to find user
     */
    public void removeUser(int userID){
        String statement = "DELETE from USER where userID="+userID+";";
        
        this.executeSQLQueryStatement(statement);
        
        System.out.println("Removed User with id = "+userID);
    }
    
    /**
     * CURRENTLY RETURNS 9 DIGIT NUMBERS!!!
     * Generates a random 8 digit userID that is not in the database
     * @return 8 digit
     */
    private int generateUserID(){
        Random rng = new Random();
        
        //base id to be added to the random digits
        int baseID = (int)Math.pow(10,((double)DIGITS_IN_USERID-1));
        //it will generate a new id until it is not contained in the database
        int id;
        do {
            id = baseID + rng.nextInt(8*baseID);
        }while(userIDList.contains(id));
        
        return id;
    }
    
    /**
     * Lists all user IDs in the database
     */
    private List<Integer> listAllUserID() throws SQLException{
        List<Integer> idList = new ArrayList();
        String statement = "SELECT userID FROM USER;";
        
        ResultSet rs = this.executeSQLResultSetStatement(statement);
        while(rs.next()){
            idList.add(rs.getInt("userID"));
        }
        stmt.close();
        c.close();
        return idList;
    }
    
    
    
    /*
    Testing that functions work
    */
    public static void main( String args[] ) throws SQLException {
        UserController uC = new UserController();
        System.out.println(uC.listAllUserID());
        System.out.println("yeeeee");
        //System.out.println(uC.generateUserID());
       // uC.addUser("Mike", "Morehamn", "g@gmail.gom", 3);
       //uC.removeUser(47351529);
    }
}
