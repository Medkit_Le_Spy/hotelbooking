import java.io.IOException;
import java.sql.SQLException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Naifira
 */
public class HotelBooking extends Application {
    
    @Override
    public void start(Stage stage) throws IOException {
        
        FXMLLoader loader = new FXMLLoader(
            getClass().getResource(
                        "LoginView.fxml"
                       )
        );
        
        stage.setScene(
            new Scene(
                (Pane) loader.load()
            )
        );
        
        stage.setTitle("Hotel Booking");
       
        stage.show();
    }

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws SQLException {
        launch(args);
    }
}
