import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author yfj15xqu
 */
public class LoginViewController {
    
    @FXML
    private Button continueAsGuestButton;
    
    @FXML
    private void continueAsGuestButtonAction() throws IOException{
        this.showRoomsView();
    }
    
    private void showRoomsView() throws IOException{
        FXMLLoader loader = new FXMLLoader(
            getClass().getResource(
                        "/RoomsView.fxml"
                       )
        );
        Stage stage = new Stage();
        stage.setScene(
            new Scene(
                (Pane) loader.load()
            )
        );
        
        stage.setTitle("Rooms");
       
        stage.show();
    }
}
