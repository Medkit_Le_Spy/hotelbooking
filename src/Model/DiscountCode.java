/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Team 123
 */
public class DiscountCode {

    String discountCode;    //The discount code itself
    DiscountType type;      //The type of discount code

    public DiscountCode(String discountCode, DiscountType type) {
        this.discountCode = discountCode;
        this.type = type;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public DiscountType getType() {
        return type;
    }

    public void setType(DiscountType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "DiscountCode{" + "Discount Code:" + discountCode + ", Discount Type:" + type + '}';
    }
    
    
}
