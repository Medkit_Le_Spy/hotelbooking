/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Team 123
 */
public class DiscountType {
    
    int discountTypeID; //The discount type's ID
    int percentage;     //The percentage the user saves

    public DiscountType(int discountTypeID, int percentage) {
        this.discountTypeID = discountTypeID;
        this.percentage = percentage;
    }

    public int getDiscountTypeID() {
        return discountTypeID;
    }

    public void setDiscountTypeID(int discountTypeID) {
        this.discountTypeID = discountTypeID;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    @Override
    public String toString() {
        return "DiscountType{" + "Discount Type ID: " + discountTypeID + ", Percentage: " + percentage + '}';
    }

    
}
