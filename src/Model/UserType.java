/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Team 123
 */
public class UserType {
    
    private int userTypeID;         //ID indicating a user's type
    private String type;            //String describing the type of user 

    public UserType(int userTypeID, String type) {
        this.userTypeID = userTypeID;
        this.type = type;
    }

    public int getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(int userTypeID) {
        this.userTypeID = userTypeID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UserType{" + "User Type ID: " + userTypeID + ", UserType: " + type + '}';
    }
   
    
}
