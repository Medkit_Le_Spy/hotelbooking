package Model;

public class User {

    private int userID;         //The user's ID   
    private String fName;       //THe user's first name
    private String lName;       //The user's last name
    private String email;       //The user's email
    private String password;    //The user's password
    private int cardID;         //The ID of the user's payment information
    private int userTypeID;     //The ID of the user type

    public User(int userID, String firstName, String lastName, String email, 
                int cardID, int userTypeID){
        this.userID = userID;
        this.fName = firstName;
        this.lName = lastName;
        this.email = email;
        this.userTypeID = userTypeID;
        this.cardID = cardID;
    }
    public User(int userID, String firstName, String lastName, String email, 
                String password, int cardID, int userTypeID) {
        this.userID = userID;
        this.fName = firstName;
        this.lName = lastName;
        this.email = email;
        this.password = password;
        this.userTypeID = userTypeID;
        this.cardID = cardID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCardID() {
        return cardID;
    }

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public int getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(int userTypeID) {
        this.userTypeID = userTypeID;
    }    

    @Override
    public String toString() {
        return "User{" + "User ID: " + userID + ", First Name: " + fName + ", Last Name: " + lName + ", Email Address: " + email + ", Password: " + password + ", Card ID: " + cardID + ", User Type ID: " + userTypeID + '}';
    }
    
    
}
