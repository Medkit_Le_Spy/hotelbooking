/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author Team 123
 */
public class CardInformation {

    private int cardID;         //The ID of the card
    private String holderName;  //The cardholder's name
    private int cardNo;         //The card number      
    private Date expiry;        //The expiry date on the card

    public CardInformation(int cardID, String holderName, int cardNo, Date expiry) {
        this.cardID = cardID;
        this.holderName = holderName;
        this.cardNo = cardNo;
        this.expiry = expiry;
    }

    public int getCardID() {
        return cardID;
    }

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public int getCardNo() {
        return cardNo;
    }

    public void setCardNo(int cardNo) {
        this.cardNo = cardNo;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    @Override
    public String toString() {
        return "CardInformation{" + "Card ID: " + cardID + ", Cardholder Name: " + holderName + ", CardNumber: " + cardNo + ", Expiry Date: " + expiry + '}';
    }
    
    
}
