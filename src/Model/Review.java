/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;
        
/**
 *
 * @author Team 123
 */
public class Review {
    
    private int reviewID;       //ID of the review 
    private int roomID;         //ID of the room that's being reviewed
    private String reviewName;  //The name of the review
    private Date date;          //The date the review was left
    private String reviewText;  //The review text 

    public Review(int reviewID, int roomID, String reviewName, Date date, String reviewText) {
        this.reviewID = reviewID;
        this.roomID = roomID;
        this.reviewName = reviewName;
        this.date = date;
        this.reviewText = reviewText;
    }

    public int getReviewID() {
        return reviewID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public int getRoomID(){
        return roomID;
    }
    
    public void setRoomID(int roomID){
        this.roomID = roomID;
    }
    
    public String getReviewName() {
        return reviewName;
    }

    public void setReviewName(String reviewName) {
        this.reviewName = reviewName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    @Override
    public String toString() {
        return "Review{" + "Review ID:" + reviewID + ", Room ID: " + roomID + ", Review Name: " + reviewName + ", Date: " + date + ", Review Text: " + reviewText + '}';
    }

    
}
