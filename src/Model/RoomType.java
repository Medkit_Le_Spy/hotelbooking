/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Team 123
 */
public class RoomType {
    
    private int roomTypeID;         //The ID of the room type
    private String roomTypeName;    //The name of the room type
    private int maxGuest;           //The maximum number of guests allowed in this room

    public RoomType(int roomTypeID, String roomTypeName, int maxGuest) {
        this.roomTypeID = roomTypeID;
        this.roomTypeName = roomTypeName;
        this.maxGuest = maxGuest;
    }

    public int getRoomTypeID() {
        return roomTypeID;
    }

    public void setRoomTypeID(int roomTypeID) {
        this.roomTypeID = roomTypeID;
    }

    public String getRoomTypeName() {
        return roomTypeName;
    }

    public void setRoomTypeName(String roomTypeName) {
        this.roomTypeName = roomTypeName;
    }

    public int getMaxGuest() {
        return maxGuest;
    }

    public void setMaxGuest(int maxGuest) {
        this.maxGuest = maxGuest;
    }

    @Override
    public String toString() {
        return "RoomType{" + "Room Type ID: " + roomTypeID + ", Room Type: " + roomTypeName + ", Max Guests: " + maxGuest + '}';
    }
    
    
}
