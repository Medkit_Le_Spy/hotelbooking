/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Team 123
 */
public class Service {
   
    private int serviceID;      //The ID of the service
    private String serviceName; //The name of the service
    private int price;          //The price of the service
    private String description; //The description of the service

    public Service(int serviceID, String serviceName, int price, String description) {
        this.serviceID = serviceID;
        this.serviceName = serviceName;
        this.price = price;
        this.description = description;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Service{" + "Service ID: " + serviceID + ", Service Name: " + serviceName + ", Price: " + price + ", Description: " + description + '}';
    }
    
    
}
