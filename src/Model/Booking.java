/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */        
package Model;

import java.util.Date;

/**
 *
 * @author Team 123
 */
public class Booking {
    private enum Status {
        CANCELLED,
        PENDING,
        ACTIVE,
        DONE
    }
    
    private int bookingID;          //ID of the booking
    private int userID;             //ID of the user
    private int roomID;             //ID of the room
    private Date check_in;          //The date the customer will check in
    private Date check_out;         //The date the customer will check out
    private Service[] services;     //An array containing any extra services the user has requested
    private String discountCode;    //A string for a user's discount code 
    private int noOfGuests;         //The number of guests staying
    
    public Booking(){}
    
    public Booking(int bkngID, int usrID, int rmID, Date chkin, Date chkout, Service[] srvcs, String dscntCode, int noOfGuests){
        this.bookingID = bkngID;
        this.userID = usrID;
        this.roomID = rmID;
        this.check_in = chkin;
        this.check_out = chkout;
        this.services = srvcs;
        this.discountCode = dscntCode;
        this.noOfGuests = noOfGuests;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public Date getCheckIn() {
        return check_in;
    }

    public void setCheckIn(Date check_in) {
        this.check_in = check_in;
    }

    public Date getCheckOut() {
        return check_out;
    }

    public void setCheckOut(Date check_out) {
        this.check_out = check_out;
    }

    public Service[] getServices() {
        return services;
    }

    public void setServices(Service[] services) {
        this.services = services;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public int getNoOfGuests() {
        return noOfGuests;
    }

    public void setNoOfGuests(int noOfGuests) {
        this.noOfGuests = noOfGuests;
    }

    @Override
    public String toString() {
        return "Booking{" + "Booking ID: " + bookingID + ", User ID: " + userID + ", Room ID: " + roomID + ", Check In: " + check_in + ", Check Out: " + check_out + ", Services: " + services + ", Discount Code: " + discountCode + ", Number Of Guests: " + noOfGuests + '}';
    }
    
    
}
