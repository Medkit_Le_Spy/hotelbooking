/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Team 123
 */
public class Room {
    
    private int roomID;         //The ID of the room   
    private int roomTypeID;     //The ID of the room's type
    private int roomPrice;      //The room's price
    private String[] pictures;  //String array containing the URLs to pictures of the room
    private String description; //Description of the room

    public Room(int roomID, int roomTypeID, int roomPrice, String[] pictures, String description) {
        this.roomID = roomID;
        this.roomTypeID = roomTypeID;
        this.roomPrice = roomPrice;
        this.pictures = pictures;
        this.description = description;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public int getRoomTypeID() {
        return roomTypeID;
    }

    public void setRoomTypeID(int roomTypeID) {
        this.roomTypeID = roomTypeID;
    }

    public int getRoomPrice() {
        return roomPrice;
    }

    public void setRoomPrice(int roomPrice) {
        this.roomPrice = roomPrice;
    }

    public String[] getPictures() {
        return pictures;
    }

    public void setPictures(String[] pictures) {
        this.pictures = pictures;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Room{" + "Room ID: " + roomID + ", Room Type ID: " + roomTypeID + ", Room Price: " + roomPrice + ", Pictures: " + pictures + ", Description: " + description + '}';
    }
    
    
}
