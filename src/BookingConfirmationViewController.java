
import java.util.Date;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Naifira
 */
public class BookingConfirmationViewController {
    @FXML
    private AnchorPane content;
    @FXML
    private Label dateLabel;
    
    @FXML
    private void continueButtonAction(){
        content.getScene().getWindow().hide();
    }
    
    public void initData(String checkIn){
        dateLabel.setText(checkIn);
    }
}
