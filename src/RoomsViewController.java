
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Naifira
 */
public class RoomsViewController {
    @FXML
    private AnchorPane content;
    @FXML 
    private Button backButton;
    
    @FXML
    private Button singleSuitButton;
    
    /**
     * Hides the opened window if back button is pressed
     */
    @FXML
    private void backButtonAction(){
        content.getScene().getWindow().hide();
        
    }
    
    @FXML
    private void singleSuitButtonAction() throws IOException{
        int roomTypeID = 1;
        this.showBookingView(roomTypeID);

        content.getScene().getWindow().hide();
    }
    
    private void showBookingView(int roomType) throws IOException{
        FXMLLoader loader = new FXMLLoader(
            getClass().getResource(
                        "BookingView.fxml"
                       )
        );
        Stage stage = new Stage();
        stage.setScene(
            new Scene(
                (Pane) loader.load()
            )
        );
        
        
        stage.setTitle("Booking");
        BookingViewController controller = 
                loader.<BookingViewController>getController();
        
        controller.initData(roomType);
        stage.show();
    }
    
    
}
